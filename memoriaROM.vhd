----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:06:53 06/18/2022 
-- Design Name: 
-- Module Name:    memoriaROM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity memoriaROM is
port (
		direccion: in std_logic_vector (1 downto 0);
		salida: out std_logic_vector(1 downto 0));
end memoriaROM;

architecture Behavioral of memoriaROM is
type datos is array (0 to 3) of std_logic_vector(1 downto 0);
	constant rom : datos := (
						"11", "10", "00", "01"
						);
	begin
	salida <= rom(to_integer(unsigned(direccion)));
end Behavioral;

