----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:56:07 06/18/2022 
-- Design Name: 
-- Module Name:    TB_memoriaROM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TB_memoriaROM is
end TB_memoriaROM;

architecture Behavioral of TB_memoriaROM is
COMPONENT MemoriaROM
    PORT(
         salida : OUT  std_logic_vector(1 downto 0);
         direccion : IN  std_logic_vector(1 downto 0)
        );
    END COMPONENT;
    

   --Entradas
   signal direccion : std_logic_vector(1 downto 0) := (others => '0');

 	--Salidas
   signal salida : std_logic_vector(1 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instancia la Unidad bajo prueba (UUT)
   uut: MemoriaROM PORT MAP (
          salida => salida,
          direccion => direccion
        );

   -- Clock process definition
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- Mantiene el reinicio por 100ns
      wait for 100 ns;	
		direccion <= "01";
		wait for 100 ns;
		direccion <= "00";
		wait for 100 ns;
		direccion <= "11";
		wait for 100 ns;
		direccion <= "10";
      wait;
   end process;
end;

